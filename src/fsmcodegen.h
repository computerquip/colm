/*
 * Copyright 2001-2018 Adrian Thurston <thurston@colm.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _COLM_FSMCODEGEN_H
#define _COLM_FSMCODEGEN_H

#include <ostream>

#include "fsmgraph.h"
#include "parsetree.h"
#include "redfsm.h"

/* Integer array line length. */
#define IALL 8

typedef unsigned long ulong;
typedef unsigned char uchar;

/*
 * class FsmCodeGen
 */
struct FsmCodeGen
{
public:
	FsmCodeGen( std::ostream &out, RedFsm *redFsm, fsm_tables *fsmTables );

protected:

	std::string FSM_NAME();
	std::string START_STATE_ID();
	std::ostream &ACTIONS_ARRAY();
	std::string GET_WIDE_KEY();
	std::string GET_WIDE_KEY( RedState *state );
	std::string TABS( int level );
	std::string KEY( Key key );
	std::string LDIR_PATH( char *path );
	void ACTION( std::ostream &ret, GenAction *action, int targState, bool inFinish );
	void CONDITION( std::ostream &ret, GenAction *condition );
	std::string ALPH_TYPE();
	std::string WIDE_ALPH_TYPE();
	std::string ARRAY_TYPE( unsigned long maxVal );

	std::string ARR_OFF( std::string ptr, std::string offset );
	std::string CAST( std::string type );
	std::string UINT();
	std::string GET_KEY();

	std::string ACCESS() { return "pdaRun->"; }

	std::string P() { return ACCESS() + "p"; }
	std::string PE() { return ACCESS() + "pe"; }
	std::string DATA_EOF() { return ACCESS() + "scan_eof"; }

	std::string CS();
	std::string TOP() { return ACCESS() + "top"; }
	std::string TOKSTART() { return ACCESS() + "tokstart"; }
	std::string TOKEND() { return ACCESS() + "tokend"; }
	std::string BLOCK_START() { return ACCESS() + "start"; }
	std::string TOKLEN() { return ACCESS() + "toklen"; }
	std::string ACT() { return ACCESS() + "act"; }
	std::string MATCHED_TOKEN() { return ACCESS() + "matched_token"; }

	std::string DATA_PREFIX();

	std::string START() { return DATA_PREFIX() + "start"; }
	std::string ERROR() { return DATA_PREFIX() + "error"; }
	std::string FIRST_FINAL() { return DATA_PREFIX() + "first_final"; }

	std::string ENTRY_BY_REGION() { return DATA_PREFIX() + "entry_by_region"; }


	void INLINE_LIST( std::ostream &ret, InlineList *inlineList, 
		int targState, bool inFinish );
	void EXEC_TOKEND( std::ostream &ret, InlineItem *item, int targState, int inFinish );
	void EXECTE( std::ostream &ret, InlineItem *item, int targState, int inFinish );
	void LM_SWITCH( std::ostream &ret, InlineItem *item, int targState, int inFinish );
	void SET_ACT( std::ostream &ret, InlineItem *item );
	void INIT_TOKSTART( std::ostream &ret, InlineItem *item );
	void INIT_ACT( std::ostream &ret, InlineItem *item );
	void SET_TOKSTART( std::ostream &ret, InlineItem *item );
	void SET_TOKEND( std::ostream &ret, InlineItem *item );
	void GET_TOKEND( std::ostream &ret, InlineItem *item );
	void SUB_ACTION( std::ostream &ret, InlineItem *item, int targState, bool inFinish );
	void LM_ON_LAST( std::ostream &ret, InlineItem *item );
	void LM_ON_NEXT( std::ostream &ret, InlineItem *item );
	void LM_ON_LAG_BEHIND( std::ostream &ret, InlineItem *item );
	void EXEC_TOKEND( std::ostream &ret );
	void EMIT_TOKEN( std::ostream &ret, LangEl *token );

	std::string ERROR_STATE();
	std::string FIRST_FINAL_STATE();

	std::string PTR_CONST();
	std::ostream &OPEN_ARRAY( std::string type, std::string name );
	std::ostream &CLOSE_ARRAY();
	std::ostream &STATIC_VAR( std::string type, std::string name );

	std::string CTRL_FLOW();

public:
	std::ostream &out;
	RedFsm *redFsm;
	fsm_tables *fsmTables;

	/* Write options. */
	bool dataPrefix;
	bool skipTokenLabelNeeded;

	std::ostream &TO_STATE_ACTION_SWITCH();
	std::ostream &FROM_STATE_ACTION_SWITCH();
	std::ostream &ACTION_SWITCH();
	std::ostream &STATE_GOTOS();
	std::ostream &TRANSITIONS();
	std::ostream &EXEC_FUNCS();

	unsigned int TO_STATE_ACTION( RedState *state );
	unsigned int FROM_STATE_ACTION( RedState *state );

	std::ostream &TO_STATE_ACTIONS();
	std::ostream &FROM_STATE_ACTIONS();

	void STATE_CONDS( RedState *state, bool genDefault );

	void emitSingleSwitch( RedState *state );
	void emitRangeBSearch( RedState *state, int level, int low, int high );

	std::ostream &EXIT_STATES();
	std::ostream &TRANS_GOTO( RedTrans *trans, int level );
	std::ostream &FINISH_CASES();

	void writeIncludes();
	void writeData();
	void writeInit();
	void writeExec();
	void writeCode();
	void writeMain( long activeRealm );

protected:
	/* Called from GotoCodeGen::STATE_GOTOS just before writing the gotos for
	 * each state. */
	bool IN_TRANS_ACTIONS( RedState *state );
	void GOTO_HEADER( RedState *state );
	void STATE_GOTO_ERROR();

	/* Set up labelNeeded flag for each state. */
	void setLabelsNeeded();
};

#endif /* _COLM_FSMCODEGEN_H */

